# Astro-subsites integration

This [Astro integration](https://docs.astro.build/en/guides/integrations-guide/) allows developers to treat an Astro project's folder as sub-website.

## Installation

Run the following command to install and set it up for you automatically.

```bash
astro add astro-subsites
```

## Configuration

Put a file named `domains.config.js` in the config folder at the root level of your Astro project. This file must contains the following:

```js
export default {
    'gallery': 'https://gallery.myportfolio.com',
    'www': 'https://myportfolio.com'
}
```

## Usage

In the previous example, `https://myportfolio.com` is your main Astro website. It is located at the `www` level.

Let's assume it contains a subfolder (`/gallery`) that can be fully independant. This directory must contains some assets in order to work properly for using both as a subfolder (`https://myportfolio.com/gallery`) and a subdomain (`https://gallery.myportfolio.com`).

At **production build**, this integration will look for static assets in your subsites files and make a copy of them. These files will be available directly in your subfolders.

## Authors

- aimedev (https://gitlab.com/aimedev)
