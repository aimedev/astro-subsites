import type { AstroIntegration } from "astro";
export default function AstroSubsites(): AstroIntegration;
export { AstroSubsites };
