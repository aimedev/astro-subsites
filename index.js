import * as fs from "fs";
import path from "path";
function copyAbsoluteAssets(logger, basename, destination, filespaths, recursive) {
    filespaths.forEach((filepath) => {
        if (fs.existsSync(`${basename}/${filepath}`)) {
            fs.cpSync(`${basename}/${filepath}`, `${basename}/${destination}${filepath}`);
            logger.info(`Copied file: ${destination}${filepath}`);
            if (recursive && filepath.endsWith('.css')) {
                const filesMatchesCss = findAbsoluteAssets(`${basename}/${destination}${filepath}`).map((v) => v[3]);
                copyAbsoluteAssets(logger, basename, destination, filesMatchesCss, false);
            }
        }
    });
}
function findAbsoluteAssets(filename) {
    let fileContent = fs.readFileSync(filename, { encoding: 'utf-8' });
    return [...fileContent.matchAll(/( ?(href|url|content|src)["'\(=]{1,2})(\/[^"'\)]*\.[a-zA-Z0-9]+)[\)'"]/gm)];
}
function findAbsoluteAssetsInDir(dir) {
    let absoluteAssets = [];
    let allFiles = fs.readdirSync(dir, { recursive: true, withFileTypes: true });
    allFiles.forEach((dirent) => {
        if (dirent.isDirectory())
            absoluteAssets = [...absoluteAssets, ...findAbsoluteAssetsInDir(`${dir}/${dirent.name}`)];
        else {
            absoluteAssets = [...absoluteAssets, ...findAbsoluteAssets(`${dir}/${dirent.name}`)];
        }
    });
    return absoluteAssets;
}
export default function AstroSubsites() {
    return {
        name: 'astro-subsites',
        hooks: {
            'astro:build:done': async ({ dir, logger }) => {
                const domainsConfigPath = path.resolve(dir.pathname, '../config/domains.config.js');
                if (!fs.existsSync(domainsConfigPath)) {
                    logger.warn('No domains.config.js file was found in project_dir/config directory');
                    return;
                }
                logger.info(`Found configuration file in: ${domainsConfigPath}`);
                const domainsConfig = await import(domainsConfigPath);
                Object.entries(domainsConfig.default).forEach(([key, value]) => {
                    if (key === 'www')
                        return;
                    logger.info(`Finding assets for '${key}' (${value})`);
                    const filesMatches = findAbsoluteAssetsInDir(`${dir.pathname}/${key}`).map((v) => v[3]);
                    copyAbsoluteAssets(logger, dir.pathname, key, filesMatches, true);
                });
            }
        }
    };
}
export { AstroSubsites };
